<?php

// Kairos
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Kairos', route('home'));
});

//Kairos > Historial > Proyecto
Breadcrumbs::register('history_project', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Historial', route('history_project'));
});
//Kairos > Historial > Proyecto
Breadcrumbs::register('history_date', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Historial', route('history_date'));
});
//Kairos > Historial > Proyecto
Breadcrumbs::register('history_range_date', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Historial', route('history_range_date'));
});

// Home > Crear Registros
Breadcrumbs::register('task_create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Registrar horas laboradas', route('task_create'));
});

// Home > Crear Registros
Breadcrumbs::register('task_create_range', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Registrar horas laboradas por fecha', route('task_create_range'));
});


// Home > Modificar Registros
Breadcrumbs::register('task_edit', function ($breadcrumbs) {
    $breadcrumbs->parent('history_project');
    $breadcrumbs->push('Modificar Registro', route('task_edit'));
});

// Home > Eliminar Registro
Breadcrumbs::register('task_delete', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Eliminar Categoría', route('task_delete'));
});

// Home > Copiar Registro
Breadcrumbs::register('task_copy', function ($breadcrumbs) {
    $breadcrumbs->parent('history_project');
    $breadcrumbs->push('Copiar Registro laboral', route('task_edit'));
});


// Kairos > Categorias
Breadcrumbs::register('category_view', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Ver Categorías', route('category_view'));
});

// Home > Registrar Categoría
Breadcrumbs::register('category_create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Registrar Categoría', route('category_create'));
});

// Home > Modificar Categoría
Breadcrumbs::register('category_edit', function ($breadcrumbs) {
    $breadcrumbs->parent('category_view');
    $breadcrumbs->push('Modificar Categoría', route('category_edit'));
});

// Home > Eliminar Categoría
Breadcrumbs::register('category_delete', function ($breadcrumbs) {
    $breadcrumbs->parent('category_view');
    $breadcrumbs->push('Eliminar Categoría', route('category_delete'));
});

// Kairos > Proyectos
Breadcrumbs::register('project_view', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Ver Proyectos', route('project_view'));
});

// Home > Registrar Proyecto
Breadcrumbs::register('project_create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Registrar Proyecto', route('project_create'));
});

// Home > Modificar Proyecto
Breadcrumbs::register('project_edit', function ($breadcrumbs) {
    $breadcrumbs->parent('project_view');
    $breadcrumbs->push('Modificar Proyecto', route('project_edit'));
});


// Home > Eliminar Proyecto
Breadcrumbs::register('project_delete', function ($breadcrumbs) {
    $breadcrumbs->parent('project_view');
    $breadcrumbs->push('Eliminar Proyecto', route('project_delete'));
});

// Home > Finalizar Proyecto
Breadcrumbs::register('project_end', function ($breadcrumbs) {
    $breadcrumbs->parent('project_view');
    $breadcrumbs->push('Finalizar Proyecto', route('project_end'));
});

// Home > Lider de Proyecto>
Breadcrumbs::register('leader_consult', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Proyectos Liderados', route('leader_consult'));
});

// Home > Lider de Proyecto > Reporte
Breadcrumbs::register('leader_history', function ($breadcrumbs) {
    $breadcrumbs->parent('leader_consult');
    $breadcrumbs->push('Reporte', route('leader_history'));
});
/*
// Home > Blog > [Category] > [Post]
Breadcrumbs::register('post', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('category', $post->category);
    $breadcrumbs->push($post->title, route('post', $post));
});
*/

// Kairos > Reportes
Breadcrumbs::register('report_view', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Reportes', route('report_view'));
});