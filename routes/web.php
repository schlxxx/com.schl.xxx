<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/tasks/create', 'TaskController@createByDay')->name('task_create');
Route::get('/tasks/create/week', 'TaskController@createByWeek')->name('task_create_range');
Route::get('/tasks/create/days', 'TaskController@createByDays');
Route::post('/tasks/delete','TaskController@delete')->name('task_delete');
Route::post('/tasks/delete/confirm','TaskController@destroy');
Route::post('/tasks/copy','TaskController@copyTask')->name('task_copy');
Route::post('/tasks/edit/copy','TaskController@createCopyTask');

Route::post('/tasks/edit', 'TaskController@editTask')->name('task_edit');
Route::post('/tasks/edit/update', 'TaskController@updateTask');
Route::post('/history/edit/cancel', 'TaskController@editCancel');

Route::get('/history/project', 'TaskController@historyByProject')->name('history_project');
Route::get('/history/date', 'TaskController@historyByDate')->name('history_date');
Route::get('/history/rangeDate', 'TaskController@historyByRangeDate')->name('history_range_date');
Route::get('/history/downloadPDF', 'TaskController@downloadPDF');
Route::get('/history/downloadExcel', 'TaskController@downloadExcel');

Route::post('/tasks/store', 'TaskController@store');
Route::post('/tasks/storeByRange', 'TaskController@storeByRange');
Route::post('/history', 'TaskController@history');
Route::get('/history', 'TaskController@history')->name('task_view');

//Reportes Lider
Route::get('/history/consultLeaderProject', 'TaskController@historyByProjectLeader')->name('leader_consult');
Route::get('/history/consultLeaderProjectAndCategory', 'TaskController@historyByProjectAndCategoryLeader');
Route::get('/history/leaderProjects/downloadPDF', 'TaskController@downloadLeaderPDF');
Route::get('/history/leaderProjects/downloadExcel', 'TaskController@downloadLeaderExcel');

Route::get('/history/LeaderProjects', 'TaskController@historyLeaderProjects')->name('leader_history');;

//Rutas proyectos
Route::get('/projects/view', 'ProjectController@index')->name('project_view');
Route::get('/projects/create', 'ProjectController@create')->name('project_create');
Route::get('/projects/edit', 'ProjectController@edit')->name('project_edit');
Route::post('/projects/update', 'ProjectController@update')->name('project_update');
Route::post('/projects/delete', 'ProjectController@delete')->name('project_delete');
Route::post('/projects/end', 'ProjectController@end')->name('project_end');
Route::post('/projects/store', 'ProjectController@store')->name('project_store');
Route::post('/projects/delete/confirm',  'ProjectController@destroy');
Route::post('/projects/end/confirm',  'ProjectController@endConfirm');

//Categories Routes
Route::get('/categories/create', 'CategoryController@create')->name('category_create');
Route::get('/categories/view', 'CategoryController@index')->name('category_view');

Route::post('/categories/delete', 'CategoryController@delete')->name('category_delete');
Route::post('/categories/store', 'CategoryController@store');
Route::post('/categories/edit', 'CategoryController@edit')->name('category_edit');
Route::post('/categories/update', 'CategoryController@update');
Route::post('/categories/confirm',  'CategoryController@destroy');

//Report Routes
Route::get('/report/user', 'ReportController@reportByUser')->name('report_view');
Route::resource('/report/search-history', 'ReportController');
Route::get('/report/downloadPDF', 'ReportController@downloadPDF');
Route::get('/report/downloadExcel', 'ReportController@downloadExcel');