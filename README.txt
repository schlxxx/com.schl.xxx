Run this commands after you clone the repo.
npm install
composer update

Important information.
Package structure:

app\Console = ?
app\Exceptions = Handle exceptions

app\Http\Controllers = Controllers
app\Http\Middleware = Middleware
app\Http\Requests = Handle validations

app\Models = Data ( also Domain but it don't need attributes [see database/migrations] )
app\Providers = Service Providers
app\Repositories = Business

bootstrap = bootstrap related to laravel
config = Configuration

database/factories = populating Models
database/migrations = tables, relationships and attributes
database/seeds = seed the database

node_modules = modules of node ( run npm install )

public = can be accessed by all the users ( even guests )
resources/assets = ?
resources/lang = internationalize
resources/views = *.blade.php

routes = defines the routes of the application ( usually on web.php )
storage = ?

tests = for testing purposes
vendor = composer dependencies/packages (run composer update)

.env = Sensible information, secret variables.