@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-6">
                {{ Breadcrumbs::render('category_create') }}
        </div>
    </div>

    <div class="row">
        <h1>Registrar Categoría</h1>
        <hr>
        <form action="/categories/store" method="POST">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="category_id" class="col-md-4 control-label">Código</label>
                        <input id="category_id" type="text" class="form-control" name="category_id" placeholder="Ingrese el código de la nueva categoría">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                    <label for="description" class="col-md-4 control-label">Descripción</label>
                    <input id="description" type="text" class="form-control" name="description" placeholder="Ingrese la descripción de la nueva categoría">
                    </div>
                </div>
            </div>

            <div class="col-xs-8">
                @include('layouts.errors')
            </div>

            <div class="col-xs-6">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="/categories/view" class="btn btn-danger">Cancelar</a>
                </div>
            </div>

            
        </form>
    </div>
</div>

@endsection