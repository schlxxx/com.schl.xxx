@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-6">
                {{ Breadcrumbs::render('category_edit') }}
        </div>
    </div>

    <div class="row">
        <h1>Modificar Categoría</h1>
        <hr>
        <form action="/categories/update" method="POST">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="category_id" class="col-md-4 control-label">Código</label>
                        <input id="category_id" type="text" class="form-control" name="category_id" value="{{ $category->category_id}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Descripción</label>
                        <input id="description" type="text" class="form-control" name="description" value="{{ $category->description}}">
                    </div>
                </div>
            </div>

            <div class="col-xs-8">
                @include('layouts.errors')
            </div>

            
            <div class="col-xs-8">
                <div class="form-group">
                    <input type="hidden" name="id" id="id" value="{{$category->id}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                    <a href="/categories/view" class="btn btn-danger">Cancelar</a>
                </div>
            </div>

            
        </form>
    </div>
</div>

@endsection