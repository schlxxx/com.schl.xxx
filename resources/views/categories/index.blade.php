@extends('layouts.app')

@section('content')

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">

    <div class="col-xs-12">
        {{ Breadcrumbs::render('category_view') }}
    </div>

    <div class="col-xs-12">
        @include('layouts.confirmation')
        @include('layouts.errors')
    </div>
    
    <div class="col-xs-12">
        <div style="margin-left: 76%;">
			<span>Mostrar</span>: <select id="quantity">
				<option value=""></option>
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
			</select>
		</div>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Código</th>
                <th>Descripción</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->category_id}}</td>
                    <td>{{ $category->description}}</td>
                    <td>
                        <div class="form-inline">
                            <div class="form-group">
                                <form action="/categories/edit" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{$category->id}}">
                                    <button type="submit" class="btn btn-link">Modificar</button>
                                </form>
                            </div>
                            <div class="form-group">
                                <form action="/categories/delete" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{$category->id}}">
                                    <button type="submit" class="btn btn-link">Eliminar</button>
                                </form>
                            </div>
                        </div> 
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="col-xs-12">
        <div align="center">
            {!! $categories->render() !!}
        </div>
    </div>
</div>

<script type="text/javascript">
		$(document).ready(function() {
			$("#quantity").change(function() {
				var selectedOption = $('#quantity').val();
				if (selectedOption != '') {
					window.location.replace('?limit=' + selectedOption);
				}
			});
		});
	</script>

@endsection