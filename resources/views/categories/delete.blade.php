@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-6">
                {{ Breadcrumbs::render('category_delete') }}
        </div>
    </div>

    <div class="row">
        <h1>Eliminar Categoría</h1>
        <hr>
        <p>¿Estas seguro de eliminar la siguiente categoría?

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="category_id" class="col-md-4 control-label">Código: </label>
                    <p>{{$category->category_id}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="description" class="col-md-4 control-label">Descripción: </label>
                    <p>{{ $category->description}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <form action="/categories/confirm" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="{{$category->id}}">
                        <button type="submit" class="btn btn-primary">Eliminar</button>
                        <a href="/categories/view" class="btn btn-danger">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection