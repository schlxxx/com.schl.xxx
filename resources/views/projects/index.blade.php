@extends('layouts.app')

@section('content')

    <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                    {{ Breadcrumbs::render('project_view') }}
            </div>
        </div>

        <div class="col-xs-12">
            @include('layouts.confirmation')
            @include('layouts.errors')
        </div>

        <div class="col-xs-12">
            <div style="margin-left: 76%;">
                <span>Mostrar</span>: <select id="quantity">
                    <option value=""></option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                </select>
            </div>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripci&oacute;n</th>
                    <th>L&iacute;der</th>
                    <th>Horas estimadas</th>
                    <th>Fecha inicio</th>
                    <th>Fecha fin</th>
                    <th>Acciones</th>
                </tr>
                
            </thead>
            <tbody>
                @foreach($projects as $project)
                    <tr>
                        <td>{{ $project -> name }}</td>
                        <td>{{ $project -> description }}</td>
                        <td>{{ $project -> leader -> fullName() }}</td>
                        <td>{{ $project -> estimated_hours }}</td>
                        <td>{{ $project -> start_date }}</td>
                        @if($project -> final_date)
                            <td>{{ $project -> final_date }}</td>
                        @endif
                        @if(!$project -> final_date)
                            <td>Sin Finalizar</td>
                        @endif
                        <td>
                            <div class="form-inline">
                                <div class="form-group">
                                    <form action="/projects/edit" method="GET">
                                        <input type="hidden" name="id" value="{{$project->id}}">
                                        <button type="submit" class="btn btn-link">Modificar</button>
                                    </form>
                                    <form action="/projects/delete" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{$project->id}}">
                                        <button type="submit" class="btn btn-link">Eliminar</button>
                                    </form>
                                    @if(!$project -> final_date)
                                        <form action="/projects/end" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$project->id}}">
                                            <button type="submit" class="btn btn-link">Finalizar</button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="col-xs-12">
            {!! $projects->render() !!}
        </div>
    </div>

    <script type="text/javascript">
		$(document).ready(function() {
			$("#quantity").change(function() {
				var selectedOption = $('#quantity').val();
				if (selectedOption != '') {
					window.location.replace('?limit=' + selectedOption);
				}
			});
		});
	</script>

@endsection