@extends('layouts.app')

@section('content')
    <script>
        function addToBox()
        {
            var box = document.getElementById("collaboratorsBox");
            var collaboratorsSelect = document.getElementById("collaborators");
            
            var selectedIndex = collaboratorsSelect.selectedIndex;

            var option = collaboratorsSelect.options[selectedIndex];
            if(option.value != -1){
                box.add(option)
            }
        }

        function removeFromBox()
        {
            var box = document.getElementById("collaboratorsBox");
            var collaboratorsSelect = document.getElementById("collaborators");

            var options = getSelectValues(box);
            if ( options.length != 0 ){
                var iLen = options.length;
                for(var i=0;i<iLen;i++){
                    collaboratorsSelect.add(options[i]);
                }
            }
        }

        function getSelectValues(select) {
            var result = [];
            var options = select && select.options;
            var opt;

            for (var i=0, iLen=options.length; i<iLen; i++) {
                opt = options[i];

                if (opt.selected) {
                    result.push(opt);
                }
            }
            return result;
        }

        function selectAllBoxAndSubmitForm() {
            var select = document.getElementById("collaboratorsBox");
            var form = document.getElementById("frmUpdateProject");
            var options = select && select.options;

            for (var i=0, iLen=options.length; i<iLen; i++) {
                opt = options[i];

                opt.selected=true;
            }

            form.submit();
        }
    </script>
    <div class="container">

        <div class="row">
            <div class="col-xs-6">
                {{ Breadcrumbs::render('project_edit') }}
            </div>
        </div>

        <div class="row">
            @include('layouts.errors')
            <form action="/projects/update" method="POST" id="frmUpdateProject">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id" value="{{$project->id}}">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="startDate">Fecha de inicio:</label>
                        <input type="date" name="startDate" id="startDate" class="form-control" value="{{$project->start_date}}">
                    </div>
                </div>
                <div class="col-sm-12"><p></p></div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="startDate">Horas estimadas:</label>
                        <input type="number" name="estimatedHours" id="estimatedHours" class="form-control" value="{{$project->estimated_hours}}">
                    </div>
                    <div class="form-group">
                        <label for="leader">L&iacute;der de proyecto:</label>
                        <select name="leader" id="leader" class="form-control">
                            <option value="-1">Seleccione un colaborador</option>
                            @foreach($collaborators as $collaborator)
                                <option value="{{$collaborator->id}}"> {{ $collaborator -> fullName() }} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="collaborators">Colaboradores:</label>
                        <div class="input-group">
                            <select name="collaborators" id="collaborators" class="form-control">
                                <option value="-1">Seleccione un colaborador</option>
                                @foreach($collaborators as $collaborator)
                                    @if(!$project->collaborators->contains($collaborator))
                                        <option value="{{$collaborator->id}}">{{ $collaborator -> fullName() }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-success" onClick="addToBox()">+</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="collaboratorsBox">Colaboradores del proyecto:</label>
                        <div class="input-group">
                            <select multiple size="3" name="collaboratorsBox[]" id="collaboratorsBox" class="form-control">
                                @foreach($project->collaborators as $collaborator)
                                    <option value="{{$collaborator->id}}">{{ $collaborator -> fullName() }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger" onClick="removeFromBox()">-</button>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Nombre:</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$project->name}}">
                    </div>
                    <div class="form-group">
                        <label for="description">Descripci&oacute;n:</label>
                        <textarea rows="9" name="description" id="description" class="form-control">{{$project->description}}</textarea>
                    </div>
                </div>

                <div class="form-group col-sm-12 text-center">
                    <button type="button" class="btn btn-success" onClick="selectAllBoxAndSubmitForm()">Modificar</button>
                    <button type="reset" class="btn btn-danger">Cancelar</button>
                </div>
            </form>
        </div>
    </div>

@endsection