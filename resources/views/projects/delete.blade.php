@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-6">
                {{ Breadcrumbs::render('project_delete') }}
        </div>
    </div>

    <div class="row">
        <h1>Eliminar Proyecto</h1>
        <hr>
        <p>¿Est&acute; seguro de eliminar el siguiente proyecto?</p>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="code" class="col-md-4 control-label">Nombre: </label>
                    <p>{{$project->name}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="description" class="col-md-4 control-label">Descripción: </label>
                    <p>{{ $project->description}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <form action="/projects/delete/confirm" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="{{$project->id}}">
                        <button type="submit" class="btn btn-primary">Eliminar</button>
                        <a href="/projects/view" class="btn btn-danger">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection