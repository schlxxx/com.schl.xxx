@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-6">
                {{ Breadcrumbs::render('project_end') }}
        </div>
    </div>

    <div class="row">
        <h1>Finalizar Proyecto</h1>
        <hr>
        <p>¿Estas seguro de finalizar el siguiente proyecto?

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="code" class="col-md-4 control-label">Nombre: </label>
                    <p>{{$project->name}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="description" class="col-md-4 control-label">Descripción: </label>
                    <p>{{ $project->description}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <form action="/projects/end/confirm" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="{{$project->id}}">
                        <button type="submit" class="btn btn-primary">Finalizar</button>
                        <a href="/projects/view" class="btn btn-danger">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection