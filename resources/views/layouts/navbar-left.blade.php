<!-- Left Side Of Navbar -->
<ul class="nav navbar-nav">
    
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Ingresar<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/tasks/create">horas por día</a></li>
                <li><a href="/tasks/create/days">horas por rango de fecha</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Historial <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/history/project">proyecto</a></li>
                <li><a href="/history/date">fecha</a></li>
                <li><a href="/history/rangeDate">rango de fechas</a></li>
            </ul>
        </li>
        @if(!auth()->user()->leadingProjects->isEmpty())
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Proyectos Liderados <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/history/consultLeaderProject">proyecto</a></li>
                    <li><a href="/history/consultLeaderProjectAndCategory">categoría</a></li>
                </ul>
            </li>
        @endif
    &nbsp;
</ul>