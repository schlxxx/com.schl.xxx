<!-- Left Side Of Navbar -->
<ul class="nav navbar-nav">
    
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Proyectos<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/projects/view">Ver</a></li>
                <li><a href="/projects/create">Crear</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categorías<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/categories/view">Ver</a></li>
                <li><a href="/categories/create">Crear</a></li>
            </ul>
        </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Reportes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <!--<li><a href="/report/general">General</a></li>-->
                <li><a href="/report/user">Generar</a></li>
            </ul>
        </li>
    &nbsp;
</ul>