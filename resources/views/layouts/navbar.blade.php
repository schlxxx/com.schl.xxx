<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-left" href="{{ url('/') }}">
                <img src="/images/logo.png" alt="">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            @if(auth()->user())
                
                @if(auth()->user()->role->description=='Collaborator')
                    @include('layouts.navbar-left')
                @endif
                @if(auth()->user()->role->description=='Administrator')
                    @include('layouts.navbar-left-admin')
                @endif

            @endif
            @include('layouts.navbar-right')
        </div>
    </div>
</nav>