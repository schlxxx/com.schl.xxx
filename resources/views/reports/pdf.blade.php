    
<style>
table {
   width: 100%;
   text-align: left;
   border-collapse: collapse;
   margin: 0 0 1em 0;
   caption-side: top;
}
caption, td, th {
   padding: 0.3em;
}
th, td {
   border-bottom: 1px solid #000;
   border-right: 1px solid #000;
   border-top: 1px solid #000;
   border-left: 1px solid #000;
}
th:lastchild, td:lastchild {
    border-right: 0;
}
th {
   width: 25%; 
}
  
img.logo{
    width: 300px; height: 80px;
}  
</style>
    <?php date_default_timezone_set('America/Costa_Rica')?>
    <p><?php echo "Reporte generado el ".date("Y-n-j")." a las ".date("h:s").date("a");;?></p>
    <div class="container">
        <div class="row">
            <div style="background:#E1E2FF; border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000;  border-left: 1px solid #000;" id="header">
                <div style="float:left;">
                    <img style="margin-top:30px;" src="https://vignette4.wikia.nocookie.net/logopedia/images/e/e3/Xxx-movie-logo.png/revision/latest?cb=20170417193616" alt="none" width="300" height="80" >
                </div>
                <div style="margin-left:100px;" align=center>
                    <h5 align=center>XXX</h5>
                    <h5 align=center>Cédula Juridica: 1111-1111-1111</h5>
                    <h5 align=center>Teléfono: 2532-0000</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                 @if($collaboratorName != "Todos")
                <h2>Nombre:</h2><p>{{$collaboratorName}}</p>
                @endif
                <br>
                <table class="table table-striped">
                <thead>
                <tr>
                    <th>Fecha de realización</th>
                    <th>Descripción</th>
                    <th>Categoría</th>
                    <th>Colaborador</th>
                    <th>Horas Invertidas</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->realization_date}}</td>
                            <td>{{ $task->description}}</td>
                            <td>{{$task->cat}}</td>
                            <td>{{$task->name." "}}{{$task->first_last_name." "}}{{$task->second_last_name}}</td>
                            <td>{{ $task->invested_time}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>      
    </div>

