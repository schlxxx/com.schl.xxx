@extends('layouts.app')
@section('content')
<div id="content">
<div class="container">
   <div class="row">
      <div class="col-xs-6">
         {{ Breadcrumbs::render('report_view') }}
      </div>
   </div>
   <div class="row">
      {{ csrf_field() }}
      <div class="col-xs-3">
         <div class="form-group">
            <label for="invested_time">Colaboradores</label>
            <select name="project" id="collaborator" class="form-control">
               <option value="-1">Seleccione un Colaborador</option>
               <option value="-2">Todos</option>
               @foreach($collaborators as $collaborator)
               <option value="{{$collaborator->id}}">{{ $collaborator -> fullName() }}</option>
               @endforeach
            </select>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-3">
            <div class="form-group">
               <label for="invested_time">Desde</label>
               <input type="date" id="initialDate" name="start_task_date" id="start_task_date" class="form-control">
            </div>
         </div>
         <div class="col-xs-3">
            <div class="form-group">
               <label for="invested_time">Hasta</label>
               <input type="date" id="finalDate" name="end_task_date" id="end_task_date" class="form-control">
            </div>
         </div>
         <div class="col-xs-2">
            <div class="form-group">
               <br>
               <button id="aButton" class="btn btn-primary">Buscar</button>
            </div>
         </div>
      </div>
   </div>
<br>
<div id="ant" class="row" style="display:none;">
<img style=" margin-left:auto; margin-right: auto; display:block;" src="https://media.giphy.com/media/LQD10wp1nW1dm/giphy.gif"></img>
</div>
<div id="space" class="container" style="display:none;">
<table id="table" class="table table-bordered" style="max-height:50px">
            <thead>
            <tr>
                <th>Descripción</th>
                <th>Categoría</th>
                <th>Fecha</th>
                <th>Tiempo Invertido</th>             
            </tr>
            </thead>
            <tbody id="data"></tbody>
        </table>

      <div class="col-xs-12">
         <div class="form-group">
            <a href="/report/downloadExcel" class="btn btn-success" >Exportar a Excel</a>
            <a href="/report/downloadPDF" class="btn btn-danger">Exportar a PDF</a>    
         </div>
      </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<script>
   $(document).ready(function(){
   $("#aButton").click(function(){
       document.getElementById('ant').style.display ='inherit';
       var token = $("#token").val();
       var collaborator = $("#collaborator").val();
       var collaboratorSelected = document.getElementById("collaborator");
       var collaboratorName = collaboratorSelected.options[collaboratorSelected.selectedIndex].text;

       var initialDate = $("#initialDate").val();
       var finalDate = $("#finalDate").val(); 
       var table = $("#table"); 
       var aux = "";
   	$.ajax({
   		url: '/report/search-history',
   		headers: {'X-CSRF-TOKEN': token},
   		type: 'POST',
   		dataType: 'json',
   		data:{collaborator: collaborator,initDate: initialDate,fnlDate: finalDate,collaboratorName:collaboratorName},
   
   		success:function(tasks){
                  table.find("#data").remove();
                  table.append('<tbody id="data"></tbody>');
                  var dataTable = $("#data");
                  $(tasks).each(function(key, value){                    
                          for (var i in value.tasks) {
                              if (value.tasks.hasOwnProperty(i)) {
                                  var name = value.tasks[i].name +" "+ value.tasks[i].first_last_name +" "+value.tasks[i].second_last_name;
                                  if(aux!=name && collaborator== -2){
                                     dataTable.append("<br>");
                                     dataTable.append("<tr style='background-color:white;'><td>Colaborador:<br>"+name+"</td></tr>");
                                     dataTable.append("<br>");
                                  }
                                    dataTable.append("<tr><td>"+value.tasks[i].description+"</td>"+
                                                         "<td>"+value.tasks[i].cat+"</td>"+
                                                         "<td>"+value.tasks[i].realization_date+"</td>"+
                                                         "<td>"+value.tasks[i].invested_time+"</td></tr>");
                               }
                               aux=name;
                        }
                   $("#space").show();  
                   $("#ant").hide();
                  });
   		},
   		error:function(msj){  
                   alert(msj);
                   $("#ant").hide(); 
                   $("#space").show();  
   		}
         });
         
   });
   });
</script>
</div>
</div>
@endsection