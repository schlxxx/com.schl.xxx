@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <h1>Modificar hora laborada</h1>
        <hr>

        <div class="row">
            <div class="col-xs-6">
                {{ Breadcrumbs::render('task_edit') }}
            </div>
        </div>

        <div class="col-xs-12">
            @include('layouts.errors')
        </div>
        
        @if( $data )        
            <form action="/tasks/edit/update" method="POST">
                {{ csrf_field() }}
                <div class="container-fluid">
                    <div class="col-xs-3">
                        <div class="form-group">
                        <label for="project">Proyecto: </label>
                        <select name="project" id="project" class="form-control">
                            <option value="-1">Seleccione un proyecto</option>
                            @foreach($projects as $project)
                                @foreach($tasks as $task)                                
                                @endforeach
                                $task = array_get($tasks, 0);
                                @if($task->project->name == $project->name)
                                    <option selected="selected" value="{{$project->id}}">{{$project->name}}</option>
                                    @continue
                                @endif                            
                                <option value="{{$project->id}}">{{$project->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <label for="category">Categor&iacute;a: </label>
                        <select name="category" id="category" class="form-control">
                            <option value="-1">Seleccione una categor&iacute;a</option>
                            @foreach($categories as $category)
                                $task = array_get($tasks, 0);
                                @if($task->category->description == $category->description)
                                    <option selected="selected" value="{{$category->id}}">{{$category->description}}</option>
                                    @continue
                                @endif                            
                                <option value="{{$category->id}}">{{$category->description}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <br>

                <div class="container-fluid">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="date">Fecha:</label>
                            @foreach($tasks as $task)
                                <input type="date" name="date" id="date" required value="{{ $task->realization_date}}" class="form-control">
                                <input type="hidden" name="created_at" id="created_at" value="{{ $task->created_at}}">
                                <input type="hidden" name="id" id="id" value="{{ $task->id}}">
                            @endforeach
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="invested_time">Cantidad de horas invertidas:</label>
                            @foreach($tasks as $task)
                            <input type="number" name="invested_time" id="invested_time" min=1 max=24 required value="{{ $task->invested_time}}" class="form-control">
                            @endforeach                    
                        </div>
                    </div> 
                </div>            

                <br>

                <div class="container-fluid">
                    <div class="form-group col-xs-12">
                        <label for="description">Descripci&oacute;n de lo realizado:</label>
                        @foreach($tasks as $task)
                            <textarea name="description" id="description" cols="30" rows="10" required class="form-control" style="max-width:50%; max-height:100px;">{{$task->description}}</textarea>
                        @endforeach
                    </div>                
                </div>

                <br>

                <div class="container-fluid">
                    <div class="col-xs-12">
                        @include('layouts.errors')
                        @include('layouts.confirmation')
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Modificar</button>
                            <a href="/" class="btn btn-danger">Cancelar</a>
                        </div>
                    </div>
                </div>
            </form>
        @endif
    </div>
</div>
@endsection