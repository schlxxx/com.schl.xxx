@extends('layouts.app')

@section('content')

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">

    <div class="col-xs-12">
        {{ Breadcrumbs::render('leader_history') }}
    </div>
    
    <div class="row">
        <h1>Proyectos Liderados</h1>
        <hr>
        
        <div class="col-xs-12">
            <div style="margin-left: 76%; padding-bottom: 10px;">
                <span>Mostrar</span>: <select id="quantity">
                    <option value=""></option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                </select>
            </div>
        </div>

        <div class="container col-xs-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Descripción</th>
                    <th>Categoría</th>
                    <th>Encargado</th>
                    <th>Fecha</th>
                    <th>Tiempo Invertido</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->description}}</td>
                            <td>{{$task->category->description}}</td>
                            <td>{{$task->collaborator->name.' '.$task->collaborator->first_last_name}}</td>
                            <td>{{ $task->realization_date}}</td>
                            <td>{{ $task->invested_time}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="col-xs-12">
                <div align="center">
                    {!! $tasks->appends(Request::capture()->except('page'))->render() !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="/history/leaderProjects/downloadExcel" class="btn btn-success">Exportar a Excel</a>
                <a href="/history/leaderProjects/downloadPDF" class="btn btn-danger">Exportar a PDF</a>    
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#quantity").change(function() {
            var selectedOption = $('#quantity').val();
            if (selectedOption != '') {

                var arr = getUrlVars();
                var URL = '?'
                var project = arr['project'];
                var category = arr['category'];
                if (project != null){
                    URL += 'project='+project;
                }
                if(category != null){
                    URL += '&category='+category;
                }
                URL += '&limit=' + selectedOption;
                window.location.replace(URL);
            }
        });
    });
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
</script>
@endsection