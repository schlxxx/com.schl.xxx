@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">  
        <div class="col-xs-6">
            {{ Breadcrumbs::render('task_create_range') }}
        </div>
    </div>
    <div class="row">
        <h1>Ingresar horas realizadas en un rango de fechas</h1>
        <hr>
        <form action="/tasks/storeByRange" method="POST">
            {{ csrf_field() }}

            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-3">
                        <label for="project">Proyecto: </label>
                        <select name="project" id="project" class="form-control">
                        <option value="-1">Seleccione un proyecto</option>
                            @foreach($projects as $project)
                                <option value="{{$project->id}}">{{$project->name}}</option>
                            @endforeach
                        </select>
                    </div>
                
                    <div class="col-xs-3">
                        <label for="category">Categor&iacute;a: </label>
                        <select name="category" id="category" class="form-control">
                            <option value="-1">Seleccione una categor&iacute;a</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->description}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>   

            <br>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="invested_time">Inicio de las horas laboradas:</label>
                            <input type="date" name="start_task_date" id="start_task_date" required value=<?php echo \Carbon\Carbon::now()->toDateString() ?> class="form-control">
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="invested_time">Finalizaci&oacute;n de las horas laboradas:</label>
                            <input type="date" name="end_task_date" id="end_task_date" required value=<?php echo \Carbon\Carbon::now()->toDateString() ?> class="form-control">
                        </div>
                    </div>
                </div>
            </div>                        
            
            <div class="col-xs-3">
                <div class="form-group">
                    <label for="invested_time">Tiempo invertido (en horas):</label>
                    <input type="number" name="invested_time" id="invested_time" required placeholder="0 horas" min=1 max=24 class="form-control">
                </div>
            </div>

            @if( $investedTime )
                <div class="col-xs-6">
                    <h2>Usted ha invertido {{ $investedTime }} horas hoy</h2>
                </div>
            @endif

            <div class="form-group col-xs-12">
                <label for="description">Ingrese una descripci&oacute;n de lo realizado:</label>
                <textarea name="description" id="description" cols="30" rows="10" required class="form-control" placeholder="Descripción"
                style="max-width:50%; max-height:100px;"></textarea>
            </div>                                    

            <div class="col-xs-12">
                @include('layouts.errors')
                @include('layouts.confirmation')
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="/" class="btn btn-danger">Cancelar</a>
                </div>
            </div>                  
        </form>
    </div>
</div>

@endsection