@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <h1>Copiar registro laboral</h1>
        <hr>

        <div class="row">
            <div class="col-xs-6">
                {{ Breadcrumbs::render('task_copy') }}
            </div>
        </div>

        <div class="col-xs-12">
            @include('layouts.errors')
        </div>
        
        @if( $data )        
            <form action="/tasks/edit/copy" method="POST">
                {{ csrf_field() }}
                <div class="container-fluid">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="project">Proyecto: </label>
                            <input type="hidden" name="project" id="project" required value="{{$task->project->id}}">                        
                            <input type="text" name="projectName" id="projectName" required value="{{$task->project->name}}" class="form-control" readonly style="background-color: white;">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <label for="category">Categor&iacute;a: </label>
                        <input type="hidden" name="category" id="category" required value="{{$task->category->id}}">
                        <input type="text" name="categoryDescription" id="categoryDescription" required value="{{$task->category->description}}" class="form-control" readonly style="background-color: white;">
                    </div>
                </div>

                <br>

                <div class="container-fluid">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="date">Fecha:</label>                            
                            <input type="date" name="date" id="date" required value="{{ $task->realization_date}}" class="form-control">
                            <input type="hidden" name="realization_date" id="realization_date" value="{{ $task->realization_date}}">
                            <input type="hidden" name="created_at" id="created_at" value="{{ $task->created_at}}">
                            <input type="hidden" name="id" id="id" value="{{ $task->id}}">                            
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="invested_time">Cantidad de horas invertidas:</label>
                            <input type="number" name="invested_time" id="invested_time" min=1 max=24 required value="{{ $task->invested_time}}" class="form-control" readonly style="background-color: white;">
                        </div>
                    </div> 
                </div>            

                <br>

                <div class="container-fluid">
                    <div class="form-group col-xs-12">
                        <label for="description">Descripci&oacute;n de lo realizado:</label>
                            <textarea name="description" id="description" cols="30" rows="10" required class="form-control" style="max-width:50%; max-height:100px; background-color: white;" readonly>{{$task->description}}</textarea>
                    </div>                
                </div>

                <br>

                <div class="container-fluid">
                    <div class="col-xs-12">
                        @include('layouts.confirmation')
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Copiar</button>                                                                                      
                            <a href="/history?project={{$task->project->id}}" class="btn btn-danger">Cancelar</a>                            
                        </div>
                    </div>
                </div>
            </form>
        @endif
    </div>
</div>
@endsection