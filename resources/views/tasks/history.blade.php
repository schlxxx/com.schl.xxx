@extends('layouts.app')
@section('content')

<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">

   <div class="row">
      <h1>Historial</h1>
      <hr>
      <div class="row">
        <div class="col-xs-6">
            {{ Breadcrumbs::render('history_project') }}
        </div>
      </div>

        <div class="col-xs-12">
            @include('layouts.confirmation')        
        </div>

      <form action="/history" method="GET">
         
         <div class="col-xs-4">
            <div class="form-group">
               <label for="project">Proyecto: </label>
               <select name="project" id="project" class="form-control">
                    <option value="-1">Seleccione un proyecto </option>
                  @foreach($projects as $project)
                  <option value="{{$project->id}}">{{$project->name}}</option>
                  @endforeach
               </select>
            </div>

         </div>
         @if( $initialDate )
         <div class="form-group col-xs-4">
            @if( $dateLabel )
            <label for="initial_date">Fecha:</label>
            @else
            <label for="initial_date">Desde:</label>
            @endif
            <input type="date" name="initial_date" id="initial_date" class="form-control">
         </div>
         @endif
         @if( $finalDate )
         <div class="form-group col-xs-4">
            <label for="final_date">Hasta:</label>
            <input type="date" name="final_date" id="final_date" class="form-control">
         </div>
         @endif

         <div class="col-xs-12">
            @include('layouts.errors')
         </div>

         <div class="col-xs-12">
            <div class="form-group">
               <button type="submit" class="btn btn-primary">Consultar</button>
            </div>
         </div>
      </form>

      </br>
      </br>

      @if( $data )
        <div class="col-xs-12">
                <div style="margin-left: 76%; padding-bottom: 10px;">
                    <span>Mostrar</span>: 
                    <select id="quantity">
                        <option value=""></option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                    </select>
                </div>
            </div>

            <div class="container col-xs-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Descripción</th>
                            <th>Categoría</th>
                            <th>Fecha</th>
                            <th>Tiempo Invertido</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ $task->description}}</td>
                                <td>{{$task->category->description}}</td>
                                <td>{{ $task->realization_date}}</td>
                                <td>{{ $task->invested_time}}</td>
                                <td>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <form action="/tasks/edit" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{$task->id}}">
                                                <button type="submit" class="btn btn-link">Modificar</button>
                                            </form>
                                        </div>
                                        <div class="form-group">
                                            <form action="/tasks/delete" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{$task->id}}">
                                                <button type="submit" class="btn btn-link">Eliminar</button>
                                            </form>
                                        </div>
                                        <div class="form-group">
                                            <form action="/tasks/copy" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{$task->id}}">
                                                <button type="submit" class="btn btn-link">Copiar</button>
                                            </form>
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="col-xs-12">
                    <div align="center">
                        {!! $tasks->appends(Request::capture()->except('page'))->render() !!}
                    </div>
                </div>
            </div>
        
            <div class="col-xs-12">
                <div class="form-group">
                    <a href="/history/downloadExcel" class="btn btn-success">Exportar a Excel</a>
                    <a href="/history/downloadPDF" class="btn btn-danger">Exportar a PDF</a>    
                </div>
            </div>
        @endif
   </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#quantity").change(function() {
            var selectedOption = $('#quantity').val();
            if (selectedOption != '') {

                var arr = getUrlVars();
                var URL = '?'
                var project = arr['project'];
                var initial_date = arr['initial_date'];
                var final_date = arr['final_date'];
                if (project != null){
                    URL += 'project='+project;
                }
                if(initial_date != null){
                    URL += '&initial_date='+initial_date;
                }
                if(final_date != null){
                    URL += '&final_date='+final_date;
                }
                URL += '&limit=' + selectedOption;
                window.location.replace(URL);
            }
        });
    });
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
</script>
@endsection