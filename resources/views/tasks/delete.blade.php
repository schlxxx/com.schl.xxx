@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                {{ Breadcrumbs::render('task_delete') }}
            </div>
        </div>

        <div class="row">
            <h1>Eliminar registro</h1>
            <hr>

            <p>¿Est&aacute; seguro de eliminar el siguiente registro?</p>

            <div class="row">
                <div class="col-xs-4">
                    <p><b>Proyecto:</b>  {{$task->project->name}}</p>
                </div>
                <div class="col-xs-4">
                        <p><b>Categor&iacute;a:</b>  {{$task->category->description}}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <p><b>Colaborador:</b> {{$task->collaborator->fullName()}}</p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                    <p><b>Fecha de realizaci&oacute;:</b> {{Carbon\Carbon::parse($task->realization_date)->toFormattedDateString()}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <p><b>Descripci&oacute;n:</b> {{$task->description}}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <p><b>Tiempo invertido:</b> {{$task->invested_time}}</p>
                        <output id="investedTime"></output>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6 text-center">
                    <div class="form-group">
                        <form action="/tasks/delete/confirm" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" id="id" value="{{$task->id}}">
                            <input type="hidden" name="project" id="project" value="{{$task->project->id}}">
                            <button type="submit" class="btn btn-primary">Eliminar</button>
                            <a href="/history?project={{$task->project->id}}" class="btn btn-danger">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection