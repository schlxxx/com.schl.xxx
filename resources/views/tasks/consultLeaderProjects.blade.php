@extends('layouts.app')

@section('content')

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">

    <div class="col-xs-12">
        {{ Breadcrumbs::render('leader_consult') }}
    </div>
    
    <div class="row">
        <h1>Proyectos Liderados</h1>
        <hr>
        <form action="/history/LeaderProjects" method="GET">
        
            <div class="col-xs-4">
                <div class="form-group">
                <label for="project">Proyecto: </label>
                <select name="project" id="project" class="form-control">
                        <option value="-1">Seleccione un proyecto</option>
                    @foreach($projects as $project)
                    <option value="{{$project->id}}">{{$project->name}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            @if( $category )
                <div class="form-group col-xs-4">
                    <div class="form-group">
                        <label for="category">Categoría: </label>
                        <select name="category" id="category" class="form-control">
                                <option value="-1">Seleccione una categoría</option>
                            @foreach($categories as $categori)
                                <option value="{{$categori->id}}">{{$categori->description}}</option>
                            @endforeach
                        </select>
                    </div>    
                </div>
            @endif

            <div class="col-xs-12">
                @include('layouts.errors')
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </form> 
    </div>
</div>

@endsection