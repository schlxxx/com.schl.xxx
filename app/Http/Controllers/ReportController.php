<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Category;
use App\Models\Project;
use App\Models\Collaborator;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Symfony\Component\Console\Input\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use PDF;
use Excel;

use PHPExcel_Worksheet_Drawing;

class ReportController extends Controller
{
    public function __construct() 
    {
      $this->middleware('auth');
    }

 public function reportByUser()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $initialDate=true;
        $finalDate=true;
        $data= false;
        $dateLabel=false;

        $collaborators = Collaborator::all()->whereNotIn('role_id','1');

        return view('reports.reportByUser', compact(['projects', 'data','initialDate','finalDate','dateLabel','collaborators']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){

        $user = auth()->user();
        $projects = $user->projects;
        $data= true;
        $initialDate=false;
        $finalDate=false;
        $dateLabel=false;

        if(request('collaborator')==-2){
         $tasks= DB::table('tasks')->join("categories","tasks.category_id","=","categories.id")->join("collaborators","tasks.collaborator_id","=","collaborators.id")->select('tasks.*','categories.description as cat','collaborators.name','collaborators.first_last_name','collaborators.second_last_name')->get();   
        }else{
         $tasks= DB::table('tasks')->join("categories","tasks.category_id","=","categories.id")->select('tasks.*','categories.description as cat')->get();   
        }
        $initialDateValue= request('initDate');
        $finalDateValue = request('fnlDate');
        $collaboratorName = request('collaboratorName');

       if($initialDateValue != null && $finalDateValue != null){
            if($initialDateValue<= $finalDateValue){
                if($initialDateValue <= Carbon::now()->toDateString()){
                        $tasks = $tasks->where('realization_date','>=',$initialDateValue);
                        $tasks = $tasks->where('realization_date','<=',$finalDateValue);
                }else{
                    return response() -> error() -> json(["msj" => 'La fecha inicial no puede ser mayor a la fecha actual']);
                }
            }else{
                return response() -> error() -> json(["msj" => 'La fecha inicial no puede ser mayor a la fecha final']);
            }   
        }


        request()->session()->put('tasksList', $tasks);
        request()->session()->put('collaboratorName', $collaboratorName);

        return response() -> json(["tasks" => $tasks]);
        }       
    }

    public function downloadPDF(){
        $user = auth()->user();
        $tasks = request()->session()->get('tasksList');
        $collaboratorName = request()->session()->get('collaboratorName');

        $pdf = PDF::loadView('reports.pdf',['tasks' => $tasks,'collaboratorName'=>$collaboratorName]);

        $filename='Reporte_Laboral_'.$collaboratorName.'.pdf';

        //carga el archivo al servidor
        file_put_contents('reports/pdf/admin/'.$filename, $pdf->output());

        return $pdf->download($filename);
    }

    public function downloadExcel(){
        $user = auth()->user();
        $tasks = request()->session()->get('tasksList');
        $collaboratorName = request()->session()->get('collaboratorName');

        Excel::create('Reporte_Laboral_'.$collaboratorName, function($excel) use($collaboratorName, $tasks) {
            
            $excel->sheet('Reporte', function($sheet) use($tasks, $collaboratorName) {
                //Encabezado
                //Logo de empresa
                $objDrawing = new PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('images/logo.png')); //your image path
                $objDrawing->setCoordinates('A2');
                $objDrawing->setWorksheet($sheet);

                //Datos encabezado
                $sheet->mergeCells('B2:D2');
                $sheet->getStyle('B2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B2','XXX');

                $sheet->getStyle('B3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B3','Cédula Juridica');
                $sheet->mergeCells('C3:D3');
                
                $sheet->getStyle('C3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('C3','1111-1111-1111');

                $sheet->getStyle('B4')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B4','Telefóno');
                
                $sheet->mergeCells('C4:D4');
                $sheet->getStyle('C4')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('C4','2532-0000');

                $sheet->mergeCells('B5:D5');
                $sheet->getStyle('B5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B5','www.xxx.com');

                $sheet->setWidth('A', 20);
                $sheet->setWidth('B', 40);
                $sheet->setWidth('C', 20);
                $sheet->setWidth('D', 20);

                $sheet->mergeCells('A7:D7');
                $sheet->row(7,['Reporte laboral: '.$collaboratorName.'']);
                $sheet->row(8,[]);
                $sheet->row(9, ['Fecha de Realización','Descripción','Categoría','Tiempo Invertido']);

                
                foreach($tasks as $task){
                    $row=[];
                    $row[0] = $task->realization_date;
                    $row[1] = $task->description;
                    $row[2] = $task->cat;
                    $row[3] = $task->invested_time;
                    $sheet->appendRow($row);
                }
            });
        })->store('xlsx','reports/excel/admin')->download('xlsx');
    }



}
