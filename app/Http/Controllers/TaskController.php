<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Category;
use App\Models\Project;

use App\Repositories\Tasks;

use Illuminate\Http\Request;
use Carbon\Carbon;

use PDF;
use Excel;

use DateTime;
use DateInterval;
use DatePeriod;

use PHPExcel_Worksheet_Drawing;

class TaskController extends Controller
{

    public function __construct() 
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createByDay()
    {
        $tasksRepository = new Tasks();

        $user = auth()->user();
        $projects = $user->projects;

        date_default_timezone_set('America/Costa_Rica');
        $projects = $projects->where('start_date','<=',Carbon::now()->toDateString());
        $categories = Category::all();

        $investedTime = $tasksRepository->GetInvestedTimeByUserForToday($user);

        return view('tasks.create', compact([
            'projects', 'categories', 'investedTime'
        ]));
    }

    public function createByWeek()
    {
        $user = auth()->user();
        $projects = $user->projects;

        date_default_timezone_set('America/Costa_Rica');
        $projects = $projects->where('start_date','<=',Carbon::now()->toDateString());
        $categories = Category::all();

        $investedTime = false;
        return view('tasks.create', compact(['projects', 'categories', 'investedTime']));
    }

    public function createByDays()
    {
        $user = auth()->user();
        $projects = $user->projects;

        date_default_timezone_set('America/Costa_Rica');
        $projects = $projects->where('start_date','<=',Carbon::now()->toDateString());
        $categories = Category::all();

        $investedTime = false;
        return view('tasks.createByRange', compact(['projects', 'categories', 'investedTime']));
    }

    public function historyByProject()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $data= false;
        $initialDate=false;
        $finalDate=false;
        $dateLabel=false;
        return view('tasks.history', compact(['projects', 'data','initialDate','finalDate','dateLabel']));
    }

     public function historyByRangeDate()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $initialDate=true;
        $finalDate=true;
        $data= false;
        $dateLabel=false;
        /*
        if (request()->session()->has('message'))
        {
            request()->session()->forget('message');
        }
        */
        return view('tasks.history', compact(['projects', 'data','initialDate','finalDate','dateLabel']));
    }

     public function historyByDate()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $initialDate=true;
        $finalDate=false;
        $data= false;
        $dateLabel=true;
        return view('tasks.history', compact(['projects', 'data','initialDate','finalDate','dateLabel']));
    }

    public function history()
    {
        date_default_timezone_set('America/Costa_Rica');
        $user = auth()->user();
        $projects = $user->projects;
        $data= true;
        $initialDate=false;
        $finalDate=false;
        $dateLabel=false;

        request()->session()->put('project', request('project'));
        request()->session()->put('initialDate',request('initial_date'));
        request()->session()->put('finalDate', request('final_date'));

        //Si existen las variables en sesion las liberamos
        if(request()->session()->has('projectId')){
            request()->session()->forget('projectId');
        }
        if(request()->session()->has('tasksList')){
            request()->session()->forget('tasksList');
        }

        $tasksSave= Task::all()->where('collaborator_id','=', $user->id);

        //Obtenemos los valores de sesion
        $idProject = request()->session()->get('project');
        $initialDateValue= request()->session()->get('initialDate');
        $finalDateValue = request()->session()->get('finalDate');

        if($initialDateValue == null && $finalDateValue == null && $idProject != -1){
            $tasksSave = $tasksSave->where('project_id','=',$idProject);
        }else if($initialDateValue != null && $finalDateValue == null && $idProject != -1){
            if($initialDateValue <= Carbon::now()->toDateString()){
                $tasksSave = $tasksSave->where('project_id','=',$idProject)->where('realization_date','=',$initialDateValue);
            }else{
                return back()->withErrors(['message' => 'La fecha ingresada no puede ser mayor a la fecha actual']);
            }

            if($idProject == -1){
                return back()->withErrors(['message' => 'Debe de seleccionar un proyecto']);
            }
        }else if($initialDateValue != null && $finalDateValue != null && $idProject != -1){
            if($initialDateValue<= $finalDateValue){
                if($initialDateValue <= Carbon::now()->toDateString()){
                    if($finalDateValue <= Carbon::now()->toDateString()){
                        $tasksSave = $tasksSave->where('project_id','=',$idProject)->where('realization_date','>=',$initialDateValue)->where('realization_date','<=',$finalDateValue);
                    }else{
                        return back()->withErrors(['message' => 'La fecha final no puede ser mayor a la fecha actual']);
                    }
                }else{
                    return back()->withErrors(['message' => 'La fecha inicial no puede ser mayor a la fecha actual']);
                }
            }else{
                return back()->withErrors(['message' => 'La fecha inicial no puede ser mayor a la fecha final']);
            }   
        }else{
            return back()->withErrors(['message' => 'Debe de seleccionar un proyecto']);
        }

        request()->session()->put('projectId', $idProject);
        request()->session()->put('tasksList', $tasksSave);
        
        //numero de elementos por pagina
        $elementQuantity = request('limit');

        //Si el objeto del get es diferente al de session y diferente de nulo lo guardamos en session
        if($elementQuantity != request()->session()->get('quantityElements') && $elementQuantity!= null){
            request()->session()->put('quantityElements', $elementQuantity);
        }

        if(request()->session()->get('quantityElements') != null){
            if($initialDateValue == null && $finalDateValue == null && $idProject != -1){
                $tasks = Task::where('collaborator_id','=', $user->id)->where('project_id','=',$idProject)->paginate(request()->session()->get('quantityElements'));
            }else if($initialDateValue != null && $finalDateValue == null && $idProject != -1){
                if($initialDateValue <= Carbon::now()->toDateString()){
                    $tasks = Task::where('collaborator_id','=', $user->id)->where('project_id','=',$idProject)->where('realization_date','=',$initialDateValue)->paginate(request()->session()->get('quantityElements'));
                }else{
                    return back()->withErrors(['message' => 'La fecha ingresada no puede ser mayor a la fecha actual']);
                }

                if($idProject == -1){
                        return back()->withErrors(['message' => 'Debe de seleccionar un proyecto']);
                }

            }else if($initialDateValue != null && $finalDateValue != null && $idProject != -1){
                if($initialDateValue <= $finalDateValue){
                    if($initialDateValue <= Carbon::now()->toDateString()){
                        if($finalDateValue <= Carbon::now()->toDateString()){
                            $tasks = Task::where('collaborator_id','=', $user->id)->where('project_id','=',$idProject)->where('realization_date','>=',$initialDateValue)->where('realization_date','<=',$finalDateValue)->paginate(request()->session()->get('quantityElements'));
                        }else{
                            return back()->withErrors(['message' => 'La fecha final no puede ser mayor a la fecha actual']);
                        }
                    }else{
                        return back()->withErrors(['message' => 'La fecha inicial no puede ser mayor a la fecha actual']);
                    }
                }else{
                    return back()->withErrors(['message' => 'La fecha inicial no puede ser mayor a la fecha final']);
                }   
            }else{
                return back()->withErrors(['message' => 'Debe de seleccionar un proyecto']);
            }
        }else{
            if($initialDateValue == null && $finalDateValue == null && $idProject != -1){
                $tasks = Task::where('collaborator_id','=', $user->id)->where('project_id','=',$idProject)->paginate(10);
            }else if($initialDateValue != null && $finalDateValue == null && $idProject != -1){
                if($initialDateValue <= Carbon::now()->toDateString()){
                    $tasks = Task::where('collaborator_id','=', $user->id)->where('project_id','=',$idProject)->where('realization_date','=',$initialDateValue)->paginate(10);
                }else{
                    return back()->withErrors(['message' => 'La fecha ingresada no puede ser mayor a la fecha actual']);
                }

                if($idProject == -1){
                        return back()->withErrors(['message' => 'Debe de seleccionar un proyecto']);
                }
            }else if($initialDateValue != null && $finalDateValue != null && $idProject != -1){
                if($initialDateValue<= $finalDateValue){
                    if($initialDateValue <= Carbon::now()->toDateString()){
                        if($finalDateValue <= Carbon::now()->toDateString()){
                            $tasks = Task::where('collaborator_id','=', $user->id)->where('project_id','=',$idProject)->where('realization_date','>=',$initialDateValue)->where('realization_date','<=',$finalDateValue)->paginate(10);
                        }else{
                            return back()->withErrors(['message' => 'La fecha final no puede ser mayor a la fecha actual']);
                        }
                    }else{
                        return back()->withErrors(['message' => 'La fecha inicial no puede ser mayor a la fecha actual']);
                    }
                }else{
                    return back()->withErrors(['message' => 'La fecha inicial no puede ser mayor a la fecha final']);
                }   
            }else{
                return back()->withErrors(['message' => 'Debe de seleccionar un proyecto']);
            }
        }

        if($tasks->isEmpty()){
            return back()->withErrors(['message' => 'No existen registros laborales relacionados con los criterios de búsqueda']);
        }else{
            return view('tasks.history', compact(['projects', 'data','initialDate','finalDate','dateLabel','tasks']));
        }
    }    

    public function downloadPDF(){
        $user = auth()->user();
        $projectId = request()->session()->get('projectId'); 
        $project = Project::find($projectId);
        $tasks = request()->session()->get('tasksList');

        $pdf = PDF::loadView('tasks.pdf',['tasks' => $tasks,'user'=>$user,'project'=>$project]);

        $filename='Reporte_Laboral_'.$user->name.'_'.$user->first_last_name.'_'
        .$user->second_last_name.'.pdf';

        //carga el archivo al servidor
        file_put_contents('reports/pdf/collaborator/'.$filename, $pdf->output());

        return $pdf->download($filename);
    }

    public function downloadExcel(){
        $user = auth()->user();
        $projectId = request()->session()->get('projectId'); 
        $project = Project::find($projectId);
        $tasks = request()->session()->get('tasksList');

        Excel::create('Reporte_Laboral_'.$user->name.'_'.$user->first_last_name.'_'
        .$user->second_last_name.'', function($excel) use($user, $tasks, $project) {
            
            $excel->sheet('Reporte', function($sheet) use($tasks, $user, $project) {
                //Encabezado
                //Logo de empresa
                $objDrawing = new PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('images/logo.png')); //your image path
                $objDrawing->setCoordinates('A2');
                $objDrawing->setWorksheet($sheet);

                //Datos encabezado
                $sheet->mergeCells('B2:D2');
                $sheet->getStyle('B2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B2','XXX');

                $sheet->getStyle('B3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B3','Cédula Juridica');
                $sheet->mergeCells('C3:D3');
                
                $sheet->getStyle('C3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('C3','1111-1111-1111');

                $sheet->getStyle('B4')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B4','Telefóno');
                
                $sheet->mergeCells('C4:D4');
                $sheet->getStyle('C4')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('C4','2532-0000');

                $sheet->mergeCells('B5:D5');
                $sheet->getStyle('B5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B5','www.xxx.com');

                $sheet->setWidth('A', 20);
                $sheet->setWidth('B', 40);
                $sheet->setWidth('C', 20);
                $sheet->setWidth('D', 20);

                $sheet->mergeCells('A7:D7');
                $sheet->row(7,['Reporte laboral: '.$user->name.' '.$user->first_last_name.' '.$user->second_last_name.'']);
                $sheet->row(8,['Proyecto: '.$project->name]);
                $sheet->row(9,[]);
                $sheet->row(10, ['Fecha de Realización','Descripción','Categoría','Tiempo Invertido']);

                
                foreach($tasks as $task){
                    $row=[];
                    $row[0] = $task->realization_date;
                    $row[1] = $task->description;
                    $row[2] = $task->category->description;
                    $row[3] = $task->invested_time;
                    $sheet->appendRow($row);
                }
            });
        })->store('xlsx','reports/excel/collaborator')->download('xlsx');
    }

    /*Metodos para la generacion de reportes de los lideres
    */

    public function historyByProjectLeader()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $categories = Category::all();
        $category = false;

        return view('tasks.consultLeaderProjects', compact(['projects','category','categories']));
    }

    public function historyByProjectAndCategoryLeader()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $categories = Category::all();
        $category = true;

        return view('tasks.consultLeaderProjects', compact(['projects','category','categories']));
    }

    public function historyLeaderProjects()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $categories = Category::all();

        request()->session()->put('projectLeader', request('project'));
        request()->session()->put('categoryLeader',request('category'));

        //Si existen las variables en sesion las liberamos
        if(request()->session()->has('projectId')){
            request()->session()->forget('projectId');
        }

        if(request()->session()->has('tasksList')){
            request()->session()->forget('tasksList');
        }

        $idProject = request()->session()->get('projectLeader');
        $idCategory = request()->session()->get('categoryLeader');

        //dd($idCategory);

        if($idCategory != -1 && $idProject != -1){
            $tasksSave= Task::all()->where('project_id','=', $idProject)->where('category_id','=',$idCategory);
        }

        if($idProject != -1 && $idCategory==null){
            $tasksSave= Task::all()->where('project_id','=', $idProject);
        }
                
        request()->session()->put('projectId', $idProject);
        request()->session()->put('tasksList', $tasksSave);

        if($idCategory == -1 && $idProject != -1){
            return back()->withErrors(['message' => 'Debe de seleccionar una categoría']);
        }else if($idCategory != -1 && $idProject == -1){
            return back()->withErrors(['message' => 'Debe de seleccionar un proyecto']);
        }else if($idCategory == -1 && $idProject == -1){
            return back()->withErrors(['message' => 'Debe de seleccionar un proyecto y una categoría']);
        }



        //numero de elementos por pagina
        $elementQuantity = request('limit');

        //Si el objeto del get es diferente al de session y diferente de nulo lo guardamos en session
        if($elementQuantity != request()->session()->get('quantityElements') && $elementQuantity!= null){
            request()->session()->put('quantityElements', $elementQuantity);
        }
        
        //si el elemento en session es diferente de nulo(se esta limitando la tabla) y no se estan mostrando todos los elementos
        if(request()->session()->get('quantityElements') != null){
            if($idCategory != null && $idProject != -1){
                $tasks= Task::where('project_id','=', $idProject)->where('category_id','=',$idCategory)->paginate(request()->session()->get('quantityElements'));
            }else if($idCategory == null && $idProject != -1){
                $tasks= Task::where('project_id','=', $idProject)->paginate(request()->session()->get('quantityElements'));
            }
        }else {//se muestran los primeros 10 elementos
            if($idCategory != null && $idProject != -1){
                $tasks= Task::where('project_id','=', $idProject)->where('category_id','=',$idCategory)->paginate(10);
            }else if($idCategory == null && $idProject != -1){
                $tasks= Task::where('project_id','=', $idProject)->paginate(10);
            }
        }

        if($tasks->isEmpty()){
            return back()->withErrors(['message' => 'No existen registros laborales relacionados con los criterios de búsqueda']);
        }else{
            return view('tasks.historyLeaderProjects', compact(['tasks']));
        }
    }

    public function downloadLeaderPDF(){
        $user = auth()->user();
        $projectId = request()->session()->get('projectId'); 
        $project = Project::find($projectId);
        $tasks = request()->session()->get('tasksList');

        $pdf = PDF::loadView('tasks.pdfLeader',['tasks' => $tasks,'user'=>$user,'project'=>$project]);

        $filename='Reporte_Proyecto_'.$project->name.'.pdf';

        //carga el archivo al servidor
        file_put_contents('reports/pdf/leader/'.$filename, $pdf->output());

        return $pdf->download($filename);
    }

    public function downloadLeaderExcel(){
        $user = auth()->user();
        $projectId = request()->session()->get('projectId'); 
        $project = Project::find($projectId);
        $tasks = request()->session()->get('tasksList');

        Excel::create('Reporte_Proyecto_'.$project->name, function($excel) use($user, $tasks, $project) {
            
            $excel->sheet('Reporte', function($sheet) use($tasks, $user, $project) {
                //Encabezado
                //Logo de empresa
                $objDrawing = new PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('images/logo.png')); //your image path
                $objDrawing->setCoordinates('A2');
                $objDrawing->setWorksheet($sheet);

                //Datos encabezado
                $sheet->mergeCells('B2:D2');
                $sheet->getStyle('B2')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B2','XXX');

                $sheet->getStyle('B3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B3','Cédula Juridica');
                $sheet->mergeCells('C3:D3');
                
                $sheet->getStyle('C3')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('C3','1111-1111-1111');

                $sheet->getStyle('B4')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B4','Telefóno');
                
                $sheet->mergeCells('C4:D4');
                $sheet->getStyle('C4')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('C4','2532-0000');

                $sheet->mergeCells('B5:D5');
                $sheet->getStyle('B5')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center')
                );
                $sheet->cell('B5','www.xxx.com');

                $sheet->setWidth('A', 40);
                $sheet->setWidth('B', 20);
                $sheet->setWidth('C', 30);
                $sheet->setWidth('D', 20);
                $sheet->setWidth('E', 20);

                $sheet->mergeCells('A7:E7');
                $sheet->row(7,['Lider de proyecto: '.$user->name.' '.$user->first_last_name.' '.$user->second_last_name.'']);
                $sheet->row(8,['Proyecto: '.$project->name]);
                $sheet->row(9,[]);
                $sheet->row(10, ['Descripción','Categoría','Encargado','Fecha','Tiempo Invertido']);

                
                foreach($tasks as $task){
                    $row=[];
                    $row[0] = $task->description;
                    $row[1] = $task->category->description;
                    $row[2] = $task->collaborator->name.' '.$task->collaborator->first_last_name;
                    $row[3] = $task->realization_date;
                    $row[4] = $task->invested_time;
                    $sheet->appendRow($row);
                }
            });
        })->store('xlsx','reports/excel/leader')->download('xlsx');
    }


    public function saveDocumentInDB($filename,$file){
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        if($extension == 'pdf'){
            $save = file_put_contents('reports/pdf/'.$filename, $file);
        }else{
            $save = file_put_contents('reports/excel/'.$filename, $file);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        date_default_timezone_set('America/Costa_Rica');
        //TODO Get the calendar selected date, also validate in business layer all the business rules.
        
        $this->validate(request(), [
            'project' => 'required|exists:projects,id',
            'description' => 'required|max:255',
            'invested_time' => 'required|numeric|min:1|max:24',
            'category' => 'required|exists:categories,id'
        ]);
        
        $user = auth()->user();
        $task = new Task;
        
        $task -> realization_date = Carbon::now();
        $task -> description = request('description');

        $task -> invested_time = request('invested_time');
        $task -> collaborator_id = $user->id;

        $task -> project_id = request('project');
        $task -> category_id = request('category');

        $task -> save();        
        return redirect('/history/project')->with(['message' => 'Registro laboral insertado con exito']);
    }

    public function storeByRange()
    {
        date_default_timezone_set('America/Costa_Rica');
        //TODO Get the calendar selected date, also validate in business layer all the business rules.                        
        
        $startTaskDate = request('start_task_date');
        $endTaskDate = request('end_task_date');

        if ($startTaskDate > $endTaskDate){
            return back()->withErrors(['message' => 'La fecha final no puede ser menor a la fecha inicial']);
        }
        $beginDate = new DateTime($startTaskDate);
        $endDate = new DateTime($endTaskDate);
        $nextDay = strtotime("+1 day", strtotime(Carbon::now()->toDateString()));
        
        $nextDay = date("Y-m-d", $nextDay);
        if ($endTaskDate > $nextDay) {
            return back()->withErrors(['message' => 'La fecha final no puede ser mayor al día de mañana']);
        }

        $this->validate(request(), [
            'project' => 'required|exists:projects,id',
            'description' => 'required|max:255',
            'invested_time' => 'required|numeric|min:1|max:24',
            'category' => 'required|exists:categories,id'
        ]);
        
        $periodOfDates = new DatePeriod($beginDate, new DateInterval('P1D'), $endDate);
        
        foreach ($periodOfDates as $date){
            $user = auth()->user();
            $task = new Task;

            $task -> realization_date = $date;
            $task -> description = request('description');
    
            $task -> invested_time = request('invested_time');
            $task -> collaborator_id = $user->id;
    
            $task -> project_id = request('project');
            $task -> category_id = request('category');
    
            $task -> save();    
        }
        return redirect('/history/project')->with(['message' => 'Registro laboral insertado con exito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */

        public function editTask ()
    {
        $taskId = request('id');
        $user = auth()->user();
        $projects = $user->projects;
        $data= true;
        //Obtenemos todas las tareas del usuario en sesion
        $tasks = Task::all()->where('id','=',$taskId);
        $categories = Category::all();

        if (request()->session()->has('tasksList'))
        {
            request()->session()->forget('tasksList');
        }
        if (request()->session()->has('message'))
        {
            request()->session()->forget('message');
        }

        request()->session()->put('tasksList', $tasks);
        
        return view('tasks.updateTask', compact(['projects', 'categories', 'data','tasks']));        
    }

    public function updateTask (){
        date_default_timezone_set('America/Costa_Rica');
        $this->validate(request(), [
            'project' => 'required|exists:projects,id',
            'description' => 'required|max:255',
            'invested_time' => 'required|numeric|min:1|max:24',
            'category' => 'required|exists:categories,id'
        ]);
        
        $user = auth()->user();
        $taskId = request('id');
        $project = request('project');
        $newTask = array(
            'id' => $taskId,
            'description' => request('description'),
            'invested_time' => request('invested_time'),
            'realization_date' => request('date'),
            'collaborator_id' => $user->id,
            'project_id' => request('project'),
            'category_id' => request('category'),
            'created_at' => request('created_at'),
            'updated_at' => Carbon::now());
        
        $task = Task::where("id", $taskId)->update($newTask);

        $projects = $user->projects;
        $tasks= Task::all()->where('collaborator_id','=', $user->id);
        $data= true;
        $initialDate=false;
        $finalDate=false;
        $dateLabel=false;        
        
        //request()->session()->put('message', 'La hora laborada ha sido modificada');
        return redirect('/history?project='.$project)->with(['message' => 'Registro laboral modificado con exito']);
    }

    public function copyTask (){
        $taskId = request('id');
        $user = auth()->user();
        $projects = $user->projects;
        $data= true;
        //Obtenemos todas las tareas del usuario en sesion
        $tasks = Task::all()->where('id','=',$taskId);
        $categories = Category::all();
        
        foreach ($tasks as $task);
        if (request()->session()->has('tasksList'))
        {
            request()->session()->forget('tasksList');
        }
        if (request()->session()->has('message'))
        {
            request()->session()->forget('message');
        }
        request()->session()->put('task', $task);
        return view('tasks.copyTask', compact(['projects', 'categories', 'data','task']));
    }

    public function createCopyTask(){
        $this->validate(request(), [
            'project' => 'required|exists:projects,id',
            'description' => 'required|max:255',
            'invested_time' => 'required|numeric|min:1|max:24',
            'category' => 'required|exists:categories,id'
        ]);
        
        $user = auth()->user();
        $project = request('project');
        $realization_date = request('realization_date');
        $date = request('date');

        if(strtotime($realization_date) == strtotime($date)){
            return redirect('/history?project='.$project)->withErrors(['message' => 'Ya existe el registro para la fecha ingresada']);            
        } else {
            date_default_timezone_set('America/Costa_Rica');
            $nextDay = strtotime("+1 day", strtotime(Carbon::now()->toDateString()));
            
            $nextDay = date("Y-m-d", $nextDay);
            if ($date > $nextDay) {
                return redirect('/history?project='.$project)->withErrors(['message' => 'No puede ingresar un registro para una fecha posterior a hoy']);            
            } else {
                $task = new Task;
                        
                $task -> description = request('description');
                $task -> invested_time = request('invested_time');
                $task -> realization_date = $date;
                $task -> collaborator_id = $user->id;
                $task -> project_id = $project;
                $task -> category_id = request('category');        

                $task -> save();        
                return redirect('/history?project='.$project)->with(['message' => 'Registro laboral insertado con exito']);
            }
        }
    }

    public function editCancel()
    {
        $user = auth()->user();
        $projects = $user->projects;
        $data= true;
        $initialDate=false;
        $finalDate=false;
        $dateLabel=false;

        $idProject = request('btnCancel');        
        $tasks= Task::all()->where('collaborator_id','=', $user->id);
        $tasks = $tasks->where('project_id','=',$idProject);
        
        if(request()->session()->has('projectId')){
            request()->session()->forget('projectId');
        }

        if(request()->session()->has('tasksList')){
            request()->session()->forget('tasksList');
        }

        request()->session()->put('projectId', $idProject);
        request()->session()->put('tasksList', $tasks);

        return view('tasks.history', compact(['projects', 'data','initialDate','finalDate','dateLabel','tasks']));
    }


    public function delete()
    {
        $id = request('id');
        $task = Task::find($id);
        return view('tasks.delete',compact('task'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $id = request('id');
        $project = request('project');
        $task = Task::find($id);
        $task -> delete();
        return redirect('/history?project='.$project)->with(['message' => 'Registro laboral eliminado con exito']);
    }
}
