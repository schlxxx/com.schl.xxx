<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Task;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct() 
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //numero de elementos por pagina
        $elementQuantity = request('limit');

        //Si el objeto del get es diferente al de session y diferente de nulo lo guardamos en session
        if($elementQuantity != request()->session()->get('quantityElements') && $elementQuantity!= null){
            request()->session()->put('quantityElements', $elementQuantity);
        }
        
        //si el elemento en session es diferente de nulo(se esta limitando la tabla) y no se estan mostrando todos los elementos
        if(request()->session()->get('quantityElements') != null){
            $categories = Category::paginate(request()->session()->get('quantityElements'));
        }else {//se muestran los primeros 10 elementos
            $categories = Category::paginate(10);
        }

        return view('categories.index', compact(['categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'category_id' => 'required|max:255',
            'description' => 'required|max:255'
        ]);

        $category = new Category;

        $category -> category_id = request('category_id');
        $category -> description = request('description');

        $categories = Category::all()->where('description','=', $category->description);

        if($categories->isEmpty()){
            $category -> save();
            return redirect('/categories/view')->with(['message' => 'Categoría registrada con exito']);
        }else{
            return back()->withErrors(['message' => 'No se puede insertar la categoría deseada. El registro ya existe en el sistema.']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id= request('id');
        $category = Category::find($id);

        return view('categories.edit', compact(['category']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $id= request('id');

        $category = Category::find($id);

        if(request('description') == null && strlen(request('description'))<=255){
            return redirect('/categories/view')->withErrors(['message' => 'El campo descripción es requerido con un tamaño máximo de 255 caracteres.']);
        }else if(request('category_id') == null && strlen(request('category_id'))<=255){
            return redirect('/categories/view')->withErrors(['message' => 'El campo código es requerido con un tamaño máximo de 255 caracteres.']);
        }else{
            $category -> category_id = request('category_id');
            $category -> description = request('description');
            $category -> save();
            return redirect('/categories/view')->with(['message' => 'Categoría modificada con exito']);
        }
    }


    public function delete(){
        $id= request('id');

        $category = Category::find($id);

        return view('categories.delete', compact(['category']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $id= request('id');

        $category = Category::find($id);

        $tasks = Task::all()->where('category_id','=', $category->id);

        if($tasks->isEmpty()){
            $category -> delete();
            return redirect('/categories/view')->with(['message' => 'Categoría eliminada con exito']);
        }else{
            return redirect('/categories/view')->withErrors(['message' => 'No se puede eliminar la categoría deseada, ya que cuenta con horas laboradas asociadas.']);
        }
    }
}
