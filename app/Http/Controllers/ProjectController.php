<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Collaborator;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Symfony\Component\Console\Input\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{

    public function __construct() 
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //numero de elementos por pagina
        $elementQuantity = request('limit');
        
        //Si el objeto del get es diferente al de session y diferente de nulo lo guardamos en session
        if($elementQuantity != request()->session()->get('quantityElements') && $elementQuantity!= null){
            request()->session()->put('quantityElements', $elementQuantity);
        }
        
        //si el elemento en session es diferente de nulo(se esta limitando la tabla) y no se estan mostrando todos los elementos
        if(request()->session()->get('quantityElements') != null){
            $projects = Project::paginate(request()->session()->get('quantityElements'));
        }else {//se muestran los primeros 10 elementos
            $projects = Project::paginate(10);
        }

        return view('projects.index',compact(['projects']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collaborators = Collaborator::all()->whereNotIn('role_id','1');
        return view('projects.create',compact(['collaborators']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'startDate' => 'required|date',
            'name' => 'required',
            'description' => 'required|max:255',
            'estimatedHours' => 'required|numeric|min:1',
            'leader' => 'required|exists:collaborators,id'
        ]);
        
        $project = new Project;

        $this->saveProject($project,$request);

        return redirect('/projects/view')->with(['message' => 'Proyecto registrado con exito']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = request('id');
        $project = Project::find($id);
        $collaborators = Collaborator::all()->whereNotIn('role_id','1');

        return view('projects.edit',compact(['project','collaborators']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(request(), [
            'startDate' => 'required|date',
            'name' => 'required',
            'description' => 'required|max:255',
            'estimatedHours' => 'required|numeric|min:1',
            'leader' => 'required|exists:collaborators,id'
        ]);

        $project = Project::find(request('id'));
        
        $this->saveProject($project, $request);
        
        return redirect('/projects/view')->with(['message' => 'Proyecto modificado con exito']);;
    }

    public function saveProject(Project $project, Request $request)
    {
        $project->name=$request->input('name');
        $project->description=$request->input('description');
        $project->start_date = $request->input('startDate');
        $project->estimated_hours=$request->input('estimatedHours');

        $project->project_leader = $request->input('leader');
        
        $collaborators = Collaborator::find($request->input('collaboratorsBox'));
        if(!$collaborators){
            return back()->withErrors(['message'=>'No se pudo modificar el proyecto, seleccione al menos un colaborador']);
        }

        $project->save();
        
        foreach ($project->collaborators as $collaborator) {
            DB::table('collaborator_project')
                ->where([
                    ['collaborator_id','=',$collaborator->id],
                    ['project_id','=',$project->id],
                    ])
                ->delete();
        }

        foreach ($collaborators as $collaborator) {
            DB::table('collaborator_project')
                ->insert(['collaborator_id'=>$collaborator->id,
                          'project_id'=>$project->id,
                          'created_at'=>Carbon::now()->toDateString(),
                          'updated_at'=>Carbon::now()->toDateString()]);
        }
    }

    public function delete()
    {
        $id = request('id');
        $project = Project::find($id);
        return view('projects.delete',compact('project'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $id = request('id');
        $project = Project::find($id);

        if($project->tasks->isEmpty()){

            foreach ($project->collaborators as $collaborator) {
                DB::table('collaborator_project')
                    ->where([
                        ['collaborator_id','=',$collaborator->id],
                        ['project_id','=',$project->id],
                        ])
                    ->delete();
            }

            $project->delete();

            return redirect('/projects/view')->with(['message' => 'Proyecto eliminado con exito']);
        } else {
            return redirect('/projects/view')->withErrors(['message' => 'No se puede eliminar el proyecto deseado, ya que cuenta con tareas asociadas.']);
        }
    }

    public function end()
    {
        $id = request('id');
        $project = Project::find($id);
        
        return view('projects.end',compact(['project']));
    }

    public function endConfirm()
    {
        $id = request('id');
        $project = Project::find($id);

        if(!$project->final_date && !$project->tasks->isEmpty()){
            $project->final_date = Carbon::now()->toDateString();
            $project->save();
            return redirect('/projects/view')->with(['message' => 'Proyecto concluido con exito']);
        }
        else {
            return redirect('/projects/view')->withErrors(['message'=> 'El proyecto no posee ninguna tarea por lo que no puede darse por finalizado.']);
        }
    }
}
