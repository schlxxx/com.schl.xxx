<?php

namespace App\Repositories;
use App\Models\Task;
use Carbon\Carbon;

class Tasks 
{

    /**
    *Get all the tasks for one user by date
    *
    *@return array \App\Models\Task
    */
    public function getTasksByUserAndDate($user, $date)
    {
        return $user -> tasks -> where('realization_date','=',$date);
    }

    /**
    *Validate the user entered hours in the selected date with the business rules
    *
    *@return bool true = success, false = error
    */
    public function ValidateEnteredHoursForUser($hours,$user,$date)
    {

        // $today = date(Carbon\Carbon::now()->toDateString());
        // $tasks = getTasksForTodayByUser($user, $today);
        $tasks = getTasksByUserAndDate($user, $date);
        $investedTime = $tasks -> sum('invested_time');
        $sumOfTimes = $investedTime + $hours;

        if ($sumOfTimes > 24) {
            return false;
        }
        else {
            return true;
        }
        
    }

    public function GetInvestedTimeByUserForToday($user)
    {
        $tasks = $this->getTasksByUserAndDate($user, Carbon::now()->toDateString());
        $investedTime = $tasks -> sum('invested_time');
        return $investedTime;
    }

}
