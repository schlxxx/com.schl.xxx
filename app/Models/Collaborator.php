<?php

namespace App\Models;

class Collaborator extends User
{
    public function projects ()
    {
        return $this->belongsToMany('App\Models\Project', 'collaborator_project');
    }

    public function role ()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function tasks ()
    {
        return $this->hasMany('App\Models\Task');
    }

    public function leadingProjects ()
    {
        return $this->hasMany('App\Models\Project','project_leader','id');
    }

    public function fullName ()
    {
        return $this -> name . ' ' . $this -> first_last_name . ' ' . $this -> second_last_name;
    }
}
