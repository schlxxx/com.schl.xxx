<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function category () 
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function project ()
    {
        return $this->belongsTo('App\Models\Project');
    }

    public function collaborator ()
    {
        return $this->belongsTo('App\Models\Collaborator');
    }
}
