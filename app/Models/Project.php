<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function tasks ()
    {
        return $this->hasMany('App\Models\Task');
    }

    public function collaborators ()
    {
        return $this->belongsToMany('App\Models\Collaborator');
    }

    public function leader ()
    {
        return $this->belongsTo('App\Models\Collaborator','project_leader','id');
    }
}
